import os
import time
import pickle

from numpy import array, load

import pypot

from explauto.sensorimotor_model.nearest_neighbor import NearestNeighbor
from explauto.environment.environment import Environment
from explauto.sensorimotor_model.ilo_gmm import IloGmm
from explauto.sensorimotor_model.imle import ImleModel
from explauto.experiment import Experiment
from explauto.utils import bounds_min_max
from explauto import InterestModel
from explauto.agent import Agent

from pyvrep.xp import PoppyVrepXp
from pyvrep.pool import VrepXpPool


scene = os.path.join(os.path.dirname(pypot.__file__),
                     '../samples/notebooks/poppy-flying.ttt')

sms = {
    'knn': (NearestNeighbor, {'sigma_ratio': 1. / 16}),
    'imle': (ImleModel, {}),
    'ilo-gmm': (IloGmm, {}),
}

motors = ['l_hip_x', 'l_hip_z', 'l_hip_y', 'l_knee_y', 'l_ankle_y']
alias = 'l_leg'

conf = {
    'motors': alias,
    'move_duration': 1.,
    'm_mins': array([-180.] * len(motors)),
    'm_maxs': array([180.] * len(motors)),
    's_mins': array([-.5, -.5, -.5]),
    's_maxs': array([.5, .5, .5]),
}

SM = ('knn', 'ilo-gmm')  # , 'imle')
IM = ('random', 'discretized_progress')
BABBLING = ('motor', 'goal')
N = 10
CPU = 3

eval_at = [2, 10, 20, 30, 50, 100, 150, 200, 250, 300, 400, 500]
tc = load('tc-25.npy')


class VrepEnvironment(Environment):
    def __init__(self, robot, motors, move_duration,
                 m_mins, m_maxs, s_mins, s_maxs):
        Environment.__init__(self, m_mins, m_maxs, s_mins, s_maxs)

        self.robot = robot
        self.motors = [m.name for m in getattr(self.robot, motors)]
        self.move_duration = move_duration

    def compute_motor_command(self, m_ag):
        m_env = bounds_min_max(m_ag, self.conf.m_mins, self.conf.m_maxs)
        return m_env

    def compute_sensori_effect(self, m_env):
        pos = dict(zip(self.motors, m_env))
        self.robot.goto_position(pos, self.move_duration, wait=True)

        time.sleep(.5)

        io = self.robot._controllers[0].io
        pos = io.get_object_position('foot_left_visual', 'base_link_visual')
        # rot = io.get_object_orientation('foot_left_visual', 'base_link_visual')

        return pos  # + rot

    def reset(self):
        self.robot.reset_simulation()


class IkXp(PoppyVrepXp):
    def __init__(self, im_name, babbling, sm_name, iter):
        PoppyVrepXp.__init__(self, scene, gui=True)

        self.im_name, self.babbling, self.sm_name = im_name, babbling, sm_name
        self.tag = 'xp-{}_{}-{}-{}.pickle'.format(im_name, babbling, sm_name, iter)

    def run(self):
        if os.path.exists('logs/{}'.format(self.tag)):
            print 'Xp', self.tag, 'already done! -> Skip'
            return

        env = VrepEnvironment(self.robot, **conf)

        im_dims = env.conf.m_dims if self.babbling == 'motor' else env.conf.s_dims
        im = InterestModel.from_configuration(env.conf, im_dims, self.im_name)

        sm_cls, kwargs = sms[self.sm_name]
        sm = sm_cls(env.conf, **kwargs)

        ag = Agent(env.conf, sm, im)

        print 'Running xp', self.tag

        xp = Experiment(env, ag)
        xp.evaluate_at(eval_at, tc)
        xp.run()

        with open('logs/{}'.format(self.tag), 'wb') as f:
            pickle.dump(xp.log, f)


if __name__ == '__main__':
    pool = VrepXpPool([IkXp(im, bab, sm, i + 1)
                       for sm in SM
                       for bab in BABBLING
                       for im in IM
                       for i in range(N)])
    pool.run(CPU)
