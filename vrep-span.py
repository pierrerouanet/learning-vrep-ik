import os
import time
import shutil
import argparse
import platform

from subprocess import Popen


VREP_PATH = '/Users/pierrerouanet/dev/v-rep/V-REP_PRO_EDU_V3_1_3_rev2b_Mac/'

VREP_BIN = {
    'Darwin': 'vrep.app/Contents/MacOS',
    'Linux': ''
}

VREP_CONFIG = """
portIndex1_port                 = {}
portIndex1_debug                = false
portIndex1_syncSimTrigger       = true
"""

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-n', '--nb-vrep', type=int, default=1)
    parser.add_argument('-p', '--port', type=int, default=19997)
    parser.add_argument('--gui', action='store_true')

    args = parser.parse_args()

    vrep_bin = os.path.join(VREP_PATH, VREP_BIN[platform.system()], 'vrep')
    vrep_args = []
    if not args.gui:
        vrep_args.append('-h')

    # Backup the config file
    cf = os.path.join(VREP_PATH, VREP_BIN[platform.system()], 'remoteApiConnections.txt')
    shutil.move(cf, cf + '.bkp')

    # Spawn the vrep instances
    processes = []

    for i in range(args.nb_vrep):
        with open(cf, 'w') as f:
            f.write(VREP_CONFIG.format(args.port + i))

        processes.append(Popen([vrep_bin, '-h']))

        time.sleep(2)

    while True:
        try:
            time.sleep(1000)
        except KeyboardInterrupt:
            break

    # Kill and clean everything
    [p.terminate() for p in processes]
    shutil.move(cf + '.bkp', cf)
